# Overview
- This is a spring boot application, implemented as a coding challenge requirement for Tarent.
- You can send a GET request to the endpoint below. Enter a string and it will be compressed if more than 2 of the same letter exist.
- Example: string entered: abzuaaissna	result: a4binssuz

# Requirements:
- Maven
- Java 8

# Install
maven clean install

# Start
- run WebserviceApplication
- edit run configuration (in intellij) and add local active profile to VM options:
``` -Dspring.profiles.active=local ```

# Endpoint
- http://localhost:8080/group/{string}
