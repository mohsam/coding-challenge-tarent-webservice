package coding.challenge.tarent.webservice.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CharGrouperTest {

    @Autowired
    CharGrouper charGrouper;

    @Test
    void group() {
        assertEquals("a4binssuz", charGrouper.group("abzuaaissna"));
    }

    // TODO: Create test class  to mock ArraySorter and spy on the behaviour of StringCompressor.
    // The services should be injected into CharGrouper...
}