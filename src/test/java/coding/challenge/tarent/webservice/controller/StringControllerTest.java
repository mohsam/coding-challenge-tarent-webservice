package coding.challenge.tarent.webservice.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(controllers = StringController.class)
class StringControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void group() throws Exception {

        String str = "abzuaaissna";
        String requestUri = "/group/" + str;

        mockMvc.perform(MockMvcRequestBuilders
               .get(requestUri))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers
                       .content()
                       .string("a4binssuz"));

    }
}