package coding.challenge.tarent.webservice.config;

import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Adds the CORS headers on local environments for the UI.
 *
 */
@Profile("local")
@Component
public class SimpleCORSFilter implements Filter {

    private static final String HOST = "Host";
    private static final String ORIGIN = "Origin";
    private static final String UTF_8 = "UTF-8";
    private final Logger logger = LoggerFactory.getLogger(SimpleCORSFilter.class);
    @Value("${cors.hosts:}")
    private String corsHosts;

    private String[] allowedHosts;

    public SimpleCORSFilter() {
        if (this.logger.isInfoEnabled()) {
            this.logger.info("SimpleCORSFilter created");
        }
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        String origin = this.getAllowOrigin(req);

        this.logger.debug(String.format("Host=%s", Encode.forJava(origin)));

        if (origin != null) {
            HttpServletResponse response = (HttpServletResponse) res;
            response.setHeader("Access-Control-Allow-Origin", Encode.forJava(origin));
            response.setHeader("Access-Control-Allow-Headers", "Authorization, content-type");
            response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, OPTIONS, PATCH");
        }

        chain.doFilter(req, res);
    }

    private String getAllowOrigin(ServletRequest req) {
        HttpServletRequest request = (HttpServletRequest) req;

        String origin = null;
        boolean contains = Arrays.stream(this.allowedHosts).anyMatch(request.getHeader(SimpleCORSFilter.HOST)::equals);
        if (contains) {
            this.logger.debug(String.format("Value '%s' from header HOST in list of allowed hosts",
                    Encode.forJava(request.getHeader(SimpleCORSFilter.HOST))));
            origin = request.getHeader(SimpleCORSFilter.HOST);
        }

        if (request.getHeader(SimpleCORSFilter.ORIGIN) != null) {
            contains = Arrays.stream(this.allowedHosts).anyMatch(request.getHeader(SimpleCORSFilter.ORIGIN)::equals);
            if (contains) {
                this.logger.debug(String.format("Value '%s' from header ORIGIN in list of allowed hosts",
                        Encode.forJava(request.getHeader(SimpleCORSFilter.HOST))));
                origin = request.getHeader(SimpleCORSFilter.ORIGIN);
            }
        }
        return origin;
    }

    @Override
    public void init(FilterConfig filterConfig) {
        this.allowedHosts = (!StringUtils.isEmpty(this.corsHosts) ? this.corsHosts.split(",") : new String[]{});
        this.logger.info(String.format("allowed hosts: %s", Arrays.toString(this.allowedHosts)));
    }

    @Override
    public void destroy() {
        // nothing to do during destroy
    }

    public void setAllowedHosts(String[] allowedHosts) {
        this.allowedHosts = allowedHosts;
    }
}