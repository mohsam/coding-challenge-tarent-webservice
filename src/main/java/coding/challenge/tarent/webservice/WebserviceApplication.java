package coding.challenge.tarent.webservice;

import coding.challenge.tarent.webservice.service.ArraySorter;
import coding.challenge.tarent.webservice.service.CharGrouper;
import coding.challenge.tarent.webservice.service.StringCompressor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"coding.challenge.tarent.webservice"})
public class WebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebserviceApplication.class, args);
	}

	@Bean
	public ArraySorter arraySorter() {
		return new ArraySorter();
	}

	@Bean
	public StringCompressor stringCompressor() {
		return new StringCompressor();
	}

	@Bean
	public CharGrouper charGrouper() {
		return new CharGrouper();
	}
}