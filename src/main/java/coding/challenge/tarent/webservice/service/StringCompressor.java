package coding.challenge.tarent.webservice.service;

public class StringCompressor {

    public String compress(String uncompressed) {

        StringBuilder stringBuilder = new StringBuilder();
        int index = 0;
        int stringSize = uncompressed.length() - 1;
        char next;
        while (index < stringSize) {
            int count = 1;
            next = uncompressed.charAt(index++);
            while (index <= stringSize && next == uncompressed.charAt(index)) {
                count++;
                index++;
            }
            if (count > 2) {
                stringBuilder.append(next);
                stringBuilder.append(count);
            } else {
                for (int j = 0; j < count; j++) {
                    stringBuilder.append(next);
                }
            }
        }
        if (index == stringSize) {
            stringBuilder.append(uncompressed.charAt(index));
        }
        return stringBuilder.toString();
    }
}